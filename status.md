# Weekly status report

---

## Week of Jun 17

### Phase
  - [X] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Gathering information about the project
  2. Dividing work

### Issues/Concerns/Road blocks
  1. plugin software was unavailable in eclipse 
  2. All the team memebers were not available 

### Action plan to address issues
  1. Downloaded plugin software and learned how to add it to eclipse

### Plans for the next week
  1. To add a plugin to eclipse

### Comments from guide (very specific)
  - Try the following suggestions to resolve issue
    1. First understand how to add a plugin to eclipse
  
---

## Week of Jun 24

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Added a random plugin to understand how to add a plugin to eclipse

### Issues/Concerns/Road blocks
  1. All team memebers were not available

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. Defining our plugin
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Jul 1

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Jul 8

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Jul 15

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Jul 22

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Jul 29

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Aug 5

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Aug 12

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. understanding datamining required for classifing from papers 
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Aug 19

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Aug 26

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [X] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Clustering understanding from Paper
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Sep 2

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [X] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Tryig to connect database to eclipse
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Sep 9

### Phase
  - [X] Team formation
  - [X] Problem definition
  - [X] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. Trying to get contents from the editor

### Issues/Concerns/Road blocks
  1. 
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Sep 16

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Sep 23

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Sep 30

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Oct 7

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Oct 14

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Oct 21

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

## Week of Oct 28

### Phase
  - [ ] Team formation
  - [ ] Problem definition
  - [ ] Implementation
  - [ ] Experimentation
  - [ ] Report writing

### Accomplishments of the week
  1. ...
  2. ...

### Issues/Concerns/Road blocks
  1. ...
  2. ...

### Action plan to address issues
  1. ...
  2. ...

### Plans for the next week
  1. ...
  2. ...

### Comments from guide (very specific)
  1. ...
  2. ...

---

